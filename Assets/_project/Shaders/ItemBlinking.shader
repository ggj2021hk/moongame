Shader "Custom/ItemBlinking"
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
        _Period ("Period" , float) = 1
	}

	SubShader 
	{
		//CurvedWorld rendertype, used by image effects
		Tags { "RenderType"="CurvedWorld_Transparent" }
        ZWrite Off
        offset -1,-1
        Blend OneMinusDstColor One
        Pass 
		{
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
#pragma multi_compile_instancing   

            #include "UnityCG.cginc"

			//CurvedWorld shader API
		#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"



			fixed4 _Color;
            float _Period;


            struct vertexInput 
			{
                float4 vertex : POSITION;
				
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct fragmentInput
			{
                float4 position : SV_POSITION;
                float3 worldPos : TEXCOORD1;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
            };

            fragmentInput vert(vertexInput i)
			{
				UNITY_SETUP_INSTANCE_ID(i);
                fragmentInput o;
				UNITY_INITIALIZE_OUTPUT(fragmentInput,o); 
				UNITY_TRANSFER_INSTANCE_ID(i, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				//CurvedWorld vertex transform
				CURVED_WORLD_TRANSFORM_POINT(i.vertex);


                o.position = UnityObjectToClipPos(i.vertex);
                 o.worldPos = mul(unity_ObjectToWorld,i.vertex);
                return o;
            }

            fixed4 frag(fragmentInput i) : SV_Target 
			{
                
                //i.position
                half a = 0.4+0.3*sin(_Time.y/_Period*2*3.1415+i.worldPos.x+i.worldPos.z);
                return _Color*a;
            }
            ENDCG
        }
    }
}

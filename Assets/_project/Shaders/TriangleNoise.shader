﻿Shader "Custom/TriangleNoise" {
	Properties {
		_GridSize("Grid Size",float) = 1
		_Color ("Color", Color) = (1,1,1,1)
		//_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Strength ("Strength", Range(0,1)) = 0.2
	}
	SubShader {
		Tags { "RenderType"="CurvedWorld_Opaque" }
		LOD 200

		CGPROGRAM
		#include "noiseSimplex.cginc"
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

		//sampler2D _MainTex;

		struct Input {
			//float2 uv_MainTex;
			float3 worldPos;
			fixed4 color:Color;
          //INTERNAL_DATA
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		half _Strength;
		float _GridSize;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void vert (inout appdata_full v, out Input o) 
		{
			UNITY_INITIALIZE_OUTPUT(Input,o); 

			//CurvedWorld vertex transform
			CURVED_WORLD_TRANSFORM_POINT_AND_NORMAL(v.vertex, v.normal, v.tangent);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			float2 pos = IN.worldPos.xz;
			pos.x-=pos.y/2;
			float2 basePos = floor(pos.xy/_GridSize);
			float extraPos = floor((pos.x+pos.y)/_GridSize);
			//o.Albedo = _Color.rgb*lerp(1-_Strength,1+_Strength,0.5+0.5*snoise(float3(basePos.x,basePos.y,extraPos)));
			o.Albedo = _Color.rgb*max(1,1-0.5*snoise(float3(basePos.x,basePos.y,extraPos)) *_Strength);
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			
			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Hidden/VacuumShaders/Curved World/VertexLit/Diffuse" 
}

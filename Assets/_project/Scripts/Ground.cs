using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GridIndex : System.IEquatable<GridIndex>
{
    public GridIndex(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public int x;
    public int y;
    public override string ToString() =>
        $"x: {x}; y: {y};";

    public bool Equals(GridIndex other)
    {
        return this.x == other.x && this.y == other.y;
    }
    public static bool operator ==(GridIndex first, GridIndex second)
    {
        return Equals(first, second);
    }
    public static bool operator !=(GridIndex first, GridIndex second)
    {
        // or !Equals(first, second), but we want to reuse the existing comparison 
        return !(first == second);
    }
}
public struct DecorationSet
{
    public int sourceIndex;
    public Vector3 position;
    public float rotation;
}

public class Grid
{
    public GridIndex index;
    public MeshRenderer groundRenderer;
    public Queue<Vector3> footprints = new Queue<Vector3>(); // x,y ,rotation
    public List<DecorationSet> decorations = new List<DecorationSet>(); // x,y ,rotation
}

public class Ground : MonoBehaviour
{
    public float gridSize = 10;
    public int gridSegment = 10;
    void OnEnable()
    {
        EventManager.AddListener("step", OnFootprint);
        EventManager.AddListener("trail", OnVehicleTrail);
    }
    void OnDisable(){

        EventManager.RemoveListener("step", OnFootprint);
        EventManager.RemoveListener("trail", OnVehicleTrail);
    }
    void Start()
    {
        PrepareMeshBasic();
        Vector3 pos = viewReference.position / gridSize;
        currentViewPosition = new GridIndex(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.z));
        UpdateDisplay();
    }

    Vector3[] vertices;
    int[] indice;
    public Material groundMaterial;
    private Dictionary<GridIndex, Grid> grids; // will remove based on length and order
    private List<Grid> displayingGrid;
    public Transform contentBase;

    public Transform viewReference;
    public int viewGridDistance;
    private GridIndex currentViewPosition;

    // i prefer meshinstance in the future for performance
    public GameObject footprintSource;
    public GameObject trailSource;
    public GameObject[] decorationSources;

    void PrepareMeshBasic()
    {
        grids = new Dictionary<GridIndex, Grid>();
        displayingGrid = new List<Grid>();

        vertices = new Vector3[6 * (gridSegment + 1) * (gridSegment + 1)];
        indice = new int[6 * gridSegment * gridSegment];

        int i = 0;
        float size = gridSize / gridSegment;
        for (int x = 0; x < gridSegment; x++)
        {
            for (int y = 0; y < gridSegment; y++)
            {
                if (y % 2 == 0)
                {
                    vertices[i] = new Vector3((x + 1) * size, 0, (y) * size);
                    vertices[i + 1] = new Vector3((x) * size, 0, (y) * size);
                    vertices[i + 2] = new Vector3((x + .5f) * size, 0, (y + 1) * size);
                    vertices[i + 3] = new Vector3((x + .5f) * size, 0, (y + 1) * size);
                    vertices[i + 4] = new Vector3((x + 1.5f) * size, 0, (y + 1) * size);
                    vertices[i + 5] = new Vector3((x + 1) * size, 0, (y) * size);
                }
                else
                {
                    vertices[i] = new Vector3((x + .5f) * size, 0, (y) * size);
                    vertices[i + 1] = new Vector3((x) * size, 0, (y + 1) * size);
                    vertices[i + 2] = new Vector3((x + 1) * size, 0, (y + 1) * size);

                    vertices[i + 3] = new Vector3((x + 1.5f) * size, 0, (y) * size);
                    vertices[i + 4] = new Vector3((x + .5f) * size, 0, (y) * size);
                    vertices[i + 5] = new Vector3((x + 1) * size, 0, (y + 1) * size);

                }
                i += 6;
            }
        }
        for (int x = 0; x < indice.Length; x++)
        {
            indice[x] = x;
        }
    }

    void Update()
    {
        Vector3 pos = viewReference.position / gridSize;
        GridIndex newGridPos = new GridIndex(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.z));
        if (newGridPos.x != currentViewPosition.x || newGridPos.y != currentViewPosition.y)
        {
            currentViewPosition = newGridPos;
            UpdateDisplay();
        }

    }

    public void UpdateDisplay()
    {

        for (int x = -2; x <= 2; x++)
        {
            for (int y = -2; y <= 2; y++)
            {
                Display(x + currentViewPosition.x, y + currentViewPosition.y);

            }
        }

    }


    void Display(int x, int y)
    {
        GridIndex index = new GridIndex(x, y);
        Grid grid = null;

        Vector3 gridPos = new Vector3(x * gridSize, 0, y * gridSize);
        GroundDeformer[] deformers = contentBase.GetComponentsInChildren<GroundDeformer>();

        if (!grids.TryGetValue(index, out grid))
        {
            grid = new Grid();
            grid.index = index;
            grid.decorations = new List<DecorationSet>();
            grids.Add(index, grid);
            if (decorationSources.Length > 0)
            {
                for (int i = 0; i < 12; i++)
                {
                    DecorationSet deco = new DecorationSet();
                    deco.rotation = Random.Range(0f, 360f);
                    deco.position = new Vector3(Random.Range(0, gridSize), 0, Random.Range(0, gridSize));

                    //brute loop for GGJ
                    bool nope = false;
                    foreach (GroundDeformer d in deformers)
                    {
                        if (Vector3.Distance(d.transform.position, gridPos + deco.position) < d.radius)
                        {
                            nope = true;
                            break;
                        }
                    }
                    if (nope) continue;
                    deco.sourceIndex = Random.Range(0, decorationSources.Length);
                    grid.decorations.Add(deco);
                }
            }

        }
        else if (grid.groundRenderer)
        {
            displayingGrid.Remove(grid);
            displayingGrid.Add(grid);
            return;
        }

        // generate mesh
        Mesh mesh = new Mesh();
        Vector3[] vs = new Vector3[vertices.Length];

        Rect rB = new Rect(x * gridSize, y * gridSize, gridSize, gridSize);
        print(x+","+y+","+rB);
        for (int i = 0; i < vertices.Length; i++)
        {
            vs[i] = vertices[i];
        }
        //brute loop for GGJ
        foreach (GroundDeformer d in deformers)
        {
            Rect rD = new Rect(d.transform.position.x - d.radius, d.transform.position.y - d.radius, d.radius * 2, d.radius * 2);
           // if (!rB.Overlaps(rD)) continue;
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3 v = vs[i];
                v += gridPos;
                v.y = 0;

                float num = Vector3.Distance(v, d.transform.position);
                if (num < d.radius - 1)
                {
                    num = d.curve.Evaluate(num / d.radius) * d.curveMultiple;
                    if (d.noise != 0) num += Mathf.PerlinNoise(v.x * 1, v.z * 1) * d.noise;
                    if (d.clampBottom) num = Mathf.Max(0, num);

                    if (vs[i].y == 0) vs[i].y = num;
                    else vs[i].y = Mathf.Max(vs[i].y, num);
                }


            }
        }
        mesh.vertices = vs;
        mesh.SetIndices(indice, MeshTopology.Triangles, 0);
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        GameObject newGrid = new GameObject(x + "," + y);
        newGrid.transform.parent = transform;
        newGrid.transform.localPosition = gridPos;
        MeshRenderer mRenderer = newGrid.AddComponent<MeshRenderer>();
        mRenderer.sharedMaterial = groundMaterial;
        MeshFilter mFilter = newGrid.AddComponent<MeshFilter>();
        mFilter.sharedMesh = mesh;

        grid.groundRenderer = mRenderer;
        displayingGrid.Add(grid);
        // generate decoraion

        foreach (DecorationSet deco in grid.decorations)
        {
            GameObject gObj = Instantiate(decorationSources[deco.sourceIndex]) as GameObject;
            gObj.transform.parent = newGrid.transform;
            gObj.transform.localPosition = deco.position;
            gObj.transform.localEulerAngles = new Vector3(0, deco.rotation, 0);
        }
    }
    void OnFootprint(object data)
    {
        object[] array = data as object[];
        Vector3 pos = (Vector3)array[0];
        Quaternion rot = (Quaternion)array[1];
        GameObject gObj = Instantiate(footprintSource);
        gObj.transform.parent = transform;
        gObj.transform.position = pos;
        gObj.transform.rotation = rot;
    }
    void OnVehicleTrail(object data)
    {
        object[] array = data as object[];
        Vector3 pos = (Vector3)array[0];
        Quaternion rot = (Quaternion)array[1];
        GameObject gObj = Instantiate(trailSource);
        gObj.transform.parent = transform;
        gObj.transform.position = pos;
        gObj.transform.rotation = rot;
    }
}

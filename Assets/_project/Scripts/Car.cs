using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VacuumShaders.CurvedWorld;

public class Car : MonoBehaviour
{
	public static Car instance{
			get; private set;
	}
    
		public float speed = 6.0f;
		public float turnSpeed = 4.0f;
		private Vector3 targetSpot;
	private Vector3 mouseDownPos;

		public GameObject clickFX;
        public Rigidbody rigidbody;

        public Transform leftTrailPoint;
        public Transform rightTrailPoint;
        
		public float trailDistance = 2;
		private Vector3 lastLeftTrail;
		private Vector3 lastRightTrail;

		public GameObject driveCharacter;
		public Animator dirCharacter;

		public AudioSource audio;
        

    void Start()
    {
        
		targetSpot = transform.position;
		lastLeftTrail = leftTrailPoint.transform.position;
		lastRightTrail= rightTrailPoint.transform.position;
    }
    void OnEnable()
    {
        
        EventManager.AddListener("suffocate",OnSuffocate);
	}


    void OnSuffocate(object data){
		driveCharacter.SetActive(false);
		dirCharacter.gameObject.SetActive(true);
            dirCharacter.Play("Suffocate");
            enabled = false;
     }
    void Update()
    {
        
			if (Input.GetMouseButtonDown(0)) {
				mouseDownPos = Input.mousePosition;
			}else if (Input.GetMouseButtonUp(0) && Vector3.Distance(Input.mousePosition,mouseDownPos)<10) {
				
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(ray.direction.y<0){

					Vector3 hitPoint = ray.origin-ray.direction* ray.origin.y/ ray.direction.y;
					
                for(int i = 0; i < 2 ; i++){
					Vector3 testPoint = CurvedWorld_Controller.current.TransformPosition(hitPoint,BEND_TYPE.Little_Planet);
					ray.origin = new Vector3(ray.origin.x,ray.origin.y+(hitPoint.y-testPoint.y),ray.origin.z);
					hitPoint = ray.origin-ray.direction* ray.origin.y/ ray.direction.y;
                }
					
					GameObject fx = Instantiate<GameObject>(clickFX);
					fx.transform.position = hitPoint;
					targetSpot = hitPoint;
				}
			}
            //
			Vector3 dir = targetSpot - transform.position;
			if(dir.magnitude>4f){
				Vector3 dir2 = Vector3.RotateTowards(transform.forward ,dir,turnSpeed*Mathf.Deg2Rad*Time.deltaTime,0);
				dir2.y = 0; // make sure
				transform.forward = dir2;
                float p = Vector3.Dot(dir.normalized,dir2.normalized);
                rigidbody.velocity =  Vector3.Lerp(rigidbody.velocity, (speed*transform.forward*p),.2f);
                
                Vector3 pos = transform.position;
                pos.y = 0 ;
                transform.position = pos;
				audio.enabled = true;
			}else{
				
				audio.enabled = false;
            }
			if(Vector3.Distance(leftTrailPoint.position,lastLeftTrail) > trailDistance){
				
				lastLeftTrail = Vector3.MoveTowards(lastLeftTrail,leftTrailPoint.position,trailDistance);
                EventManager.Trigger("trail",new object[]{lastLeftTrail, transform.rotation});

			}
			if(Vector3.Distance(rightTrailPoint.position,lastRightTrail) > trailDistance){
				
				lastRightTrail = Vector3.MoveTowards(lastRightTrail,rightTrailPoint.position,trailDistance);
                EventManager.Trigger("trail",new object[]{lastRightTrail, transform.rotation});

			}
    }
}

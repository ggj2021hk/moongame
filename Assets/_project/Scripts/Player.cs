using UnityEngine;
using System.Collections;
using VacuumShaders.CurvedWorld;

public class Player : MonoBehaviour
{

    public static Player instance
    {
        get; private set;
    }

    public Animator anim;
    public CharacterController controller;

    public float speed = 6.0f;
    public float turnSpeed = 4.0f;
    private Vector3 targetSpot;

    public GameObject clickFX;
    public float footprintDistance = 1;
    private Vector3 lastFootprint;
    private Vector3 mouseDownPos;

    private bool isAlive = true;

    void OnEnable()
    {
        instance = this;

        EventManager.AddListener("suffocate", OnSuffocate);
        EventManager.AddListener("kill", OnKill);
    }

    void OnSuffocate(object data)
    {
        anim.Play("Suffocate");
        enabled = false;
    }

    void OnKill(object data)
    {
        anim.Play("Killed");
        enabled = false;
    }
    void OnDisable()
    {
        instance = null;
        EventManager.RemoveListener("suffocate", OnSuffocate);
        EventManager.RemoveListener("kill", OnKill);
    }

    void Start()
    {
        targetSpot = transform.position;
        MakeFootprint(transform.position);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseDownPos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0) && Vector3.Distance(Input.mousePosition, mouseDownPos) < 10)
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (ray.direction.y < 0)
            {

                Vector3 hitPoint = ray.origin - ray.direction * ray.origin.y / ray.direction.y;
                for (int i = 0; i < 2; i++)
                {
                    Vector3 testPoint = CurvedWorld_Controller.current.TransformPosition(hitPoint, BEND_TYPE.Little_Planet);
                    ray.origin = new Vector3(ray.origin.x, ray.origin.y + (hitPoint.y - testPoint.y), ray.origin.z);
                    hitPoint = ray.origin - ray.direction * ray.origin.y / ray.direction.y;
                }
                GameObject fx = Instantiate<GameObject>(clickFX);
                fx.transform.position = hitPoint;
                targetSpot = hitPoint;
            }
        }

        Vector3 dir = targetSpot - transform.position;
        if (dir.magnitude > 0.1f)
        {
            anim.SetInteger("AnimationPar", 1);
            controller.Move(dir.normalized * speed * Time.deltaTime);
            Vector3 pos = transform.position;
            pos.y = 0;
            transform.position = pos;
            dir = Vector3.RotateTowards(transform.forward, dir, turnSpeed * Mathf.Deg2Rad * Time.deltaTime, 0);
            dir.y = 0; // make sure
            transform.forward = dir;
        }
        else
        {
            anim.SetInteger("AnimationPar", 0);
        }
        if (Vector3.Distance(transform.position, lastFootprint) > footprintDistance)
        {

            MakeFootprint(Vector3.MoveTowards(lastFootprint, transform.position, footprintDistance));

        }
    }

    void MakeFootprint(Vector3 position)
    {
        EventManager.Trigger("step", new object[] { position, transform.rotation });
        lastFootprint = position;
    }
    /*
    public void Die(bool killedByEnemy = false)
    {
        if (isAlive)
        {
            isAlive = false;
            Debug.Log("Die");
            // TODO: die action
            if (killedByEnemy)
            {
                anim.Play("killed");
            }
            else
            {
                anim.Play("Suffocate");
            }
        }
    }
    */
}

using UnityEngine;

public class Collectable : MonoBehaviour
{
    public enum Type {
        Collection,
        Oxygen,
        Bomb
    };

    public Type type;
    public string id;

    void OnTriggerEnter(Collider other)
    {
        // Debug.Log("OnTriggerEnter: " + other.gameObject.name);
        if (other.gameObject == PlayerManager.Instance.player) 
        {
            CollectItemManager.Instance.CollectItem(this);
            gameObject.SetActive(false);
        }
    }
}

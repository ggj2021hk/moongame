using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleDev : MonoBehaviour
{
    public float timeScale = 1;
    
    void Update()
    {
        #if UNITY_EDITOR
        Time.timeScale = timeScale;
        #endif
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public enum State
    {
        Roaming,
        ChaseTarget,
        GoingBackToStart,
    }

    // public Animator anim;
    // public CharacterController controller;

    public float speed = 6.0f;
    public float turnSpeed = 4.0f;
    public float lookRadius = 10f;
    public float targetRange = 50f;
    private Vector3 targetSpot = Vector3.zero;
    private Transform target;
    private NavMeshAgent agent;
    private Rigidbody rigidbody;
    public Vector3 startPosition;
    public Vector3 roamPosition;
    public State state;

    public float footprintDistance = 100;
    public int footprintSide = -1;
    private Vector3 lastFootprint;
    public GameObject footprintSource;
    private List<GameObject> footprintList = new List<GameObject>();

    void Awake()
    {
        // agent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
        state = State.Roaming;
    }

    void Start()
    {
        // targetSpot = transform.position;
        target = PlayerManager.Instance.player.transform;
        startPosition = transform.position;
        roamPosition = GetRoamPosition();
        FaceTarget(roamPosition);
        MakeFootprint(transform.position);
    }

    void Update()
    {
        // update movement with state machine
        switch (state)
        {
            case State.Roaming:
                MovePosition(roamPosition);
                FaceTarget(roamPosition);

                float distance = Vector3.Distance(transform.position, roamPosition);
                float reachedPositionDistance = 10f;
                // Debug.Log("distance: " + distance);
                if (distance < reachedPositionDistance)
                {
                    // Reached Roam Position
                    roamPosition = GetRoamPosition();
                    FaceTarget(roamPosition);
                }

                FindTarget();
                break;
            case State.ChaseTarget:
                float hitRange = 1f;
                if (Vector3.Distance(transform.position, target.position) < hitRange)
                {
                    // stop movement
                    FaceTarget(target.position);
                }
                else
                {
                    MovePosition(target.position);
                    FaceTarget(target.position);
                }

                float stopChaseDistance = 80f;
                if (Vector3.Distance(transform.position, target.position) > stopChaseDistance)
                {
                    // Too far, stop chasing
                    state = State.GoingBackToStart;
                }
                break;
            case State.GoingBackToStart:
                MovePosition(startPosition);
                FaceTarget(startPosition);

                reachedPositionDistance = 10f;
                if (Vector3.Distance(transform.position, startPosition) < reachedPositionDistance)
                {
                    // Reached Start Position
                    state = State.Roaming;
                }
                break;
        }

        // update movement by nav mesh agent
        // float distance = Vector3.Distance(target.position, transform.position);
        // if (distance <= lookRadius)
        // {
        //     // agent.SetDestination(target.position);
        //     // if (distance <= agent.stoppingDistance)
        //     // {
        //     //     FaceTarget();
        //     //     // TODO: attack target
        //     // }
        // }

        // update footprint
        float dist = Vector3.Distance(transform.position, lastFootprint);
        if (Vector3.Distance(transform.position, lastFootprint) > footprintDistance)
        {
            MakeFootprint(Vector3.MoveTowards(lastFootprint, transform.position, footprintDistance));
        }
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == PlayerManager.Instance.player && PlayerManager.Instance.player.GetComponent<Player>().enabled)
        {
            //PlayerManager.Instance.player.GetComponent<Player>().Die();
            EventManager.Trigger("kill");
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < 12; i++)
        {
            Gizmos.DrawLine(transform.position + Quaternion.Euler(0, i * 360 / 12, 0) * Vector3.forward * targetRange, transform.position + Quaternion.Euler(0, (i + 1) * 360 / 12, 0) * Vector3.forward * targetRange);
        }
        // Gizmos.DrawWireSphere(transform.position, lookRadius);
        // Gizmos.DrawWireSphere(transform.position, targetRange);
    }

    private void FindTarget()
    {
        if (Vector3.Distance(transform.position, target.position) < targetRange)
        {
            // Player within target range
            state = State.ChaseTarget;
        }
    }

    private void FaceTarget(Vector3 targetPos)
    {
        Vector3 direction = (targetPos - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private Vector3 GetRandomDirection()
    {
        // return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized;
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
    }

    private Vector3 GetRoamPosition()
    {
        return startPosition + GetRandomDirection() * Random.Range(10f, 70f);
    }

    private void MovePosition(Vector3 position)
    {
        Vector3 vector = position - transform.position;
        Vector3 direction = vector.normalized;
        transform.position = transform.position + (direction * (speed * Time.deltaTime));
    }

    private void MakeFootprint(Vector3 position)
    {
        // EventManager.Trigger("enemyStep", new object[] { position, transform.rotation, footprintSide });
        GameObject gObj = Instantiate(footprintSource);
        if (footprintSide == 1) gObj.transform.Find("Quad2").gameObject.SetActive(false);
        else if (footprintSide == -1) gObj.transform.Find("Quad1").gameObject.SetActive(false);
        gObj.transform.parent = GameManager.Instance.ground.gameObject.transform;
        gObj.transform.position = position;
        gObj.transform.rotation = transform.rotation;
        // footprintList.Add(gObj);
        // if (footprintList.Count > 10)
        // {
        //     GameObject firstFootprint = footprintList[0];
        //     footprintList.RemoveAt(0);
        //     // Destroy(firstFootprint);
        //     StartCoroutine(RemoveFootprint(firstFootprint));
        // }
        StartCoroutine(RemoveFootprint(gObj));
        lastFootprint = position;
        footprintSide *= -1;
    }

    IEnumerator RemoveFootprint(GameObject footprint)
    {
        yield return new WaitForSeconds(10);
        float totalTime = 2;
        float time = totalTime;
        bool didFinish = false;
        while (!didFinish)
        {
            time -= Time.deltaTime;
            if (time > 0)
            {
                Debug.Log("time:" + time / totalTime);
                Color color = new Color(72f / 255f, 72f / 255f, 72f / 255f, time / totalTime);
                // Color color = new Color(72f / 255f, 72f / 255f, 72f / 255f, 0.5f);
                footprint.transform.Find("Quad2").gameObject.GetComponent<MeshRenderer>().material.color = color;
                footprint.transform.Find("Quad1").gameObject.GetComponent<MeshRenderer>().material.color = color;
                yield return new WaitForEndOfFrame();
            }
            else
            {
                yield return new WaitForEndOfFrame();
                didFinish = true;
            }
        }
        Destroy(footprint);
    }
}
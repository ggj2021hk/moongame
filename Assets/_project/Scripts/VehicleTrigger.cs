using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleTrigger : MonoBehaviour
{
    public GameObject brokenCar;
    public GameObject completedCar;
    public GameObject controllableCar;
    public FollowTransform camSet;
bool isReady;
    public void Ready (){
        brokenCar.SetActive(false);
        completedCar.SetActive(true);
        isReady = true;

    } void OnTriggerEnter(Collider other)
    {if(!isReady) return;
        if (other.gameObject == PlayerManager.Instance.player && PlayerManager.Instance.player.GetComponent<Player>().enabled )
        {
             PlayerManager.Instance.player.SetActive(false);
             controllableCar.SetActive(true);
             camSet.target = ( controllableCar.transform);
            this.gameObject.SetActive(false);
        }
    }


}

﻿using UnityEngine;
using System.Collections;

	/// <summary>
	/// It can't be just Transform because there is extra information to set for each target.
	/// </summary>
	public class CameraRotateTarget : MonoBehaviour {

		public float maxDistance = 20;
		public float minDistance = .6f;
		[Range(-89,89)]
		public float yMinLimit = -80;
		[Range(-89,89)]
		public float yMaxLimit = 80;
		public bool limitX;
		public float xLeftLimit = -45;
		public float xRightLimit = 45;
		[Tooltip("it only matters if scaleDragWithDistance in CameraRotate is on")]
		public float scaleDragDistanceReference = 10;

		public bool relative;

		 void OnDrawGizmosSelected()
		{
			Gizmos.DrawWireSphere (transform.position,minDistance);
			Gizmos.color = new Color (1, 1, 1, .5f);
			Gizmos.DrawWireSphere (transform.position,maxDistance);
			//
			float crossSize = maxDistance+minDistance*.4f;
			Gizmos.DrawLine(transform.position+new Vector3(0,0,-crossSize),transform.position+new Vector3(0,0,crossSize));
			Gizmos.DrawLine(transform.position+new Vector3(0,-crossSize,0),transform.position+new Vector3(0,crossSize,0));
			Gizmos.DrawLine(transform.position+new Vector3(-crossSize,0,0),transform.position+new Vector3(crossSize,0,0));


			float left = 0;
			float right = 360;
			if (limitX) {
				left = 180+xLeftLimit;
				right = 180+xRightLimit;
				if(relative){
					left +=transform.eulerAngles.y;
					right +=transform.eulerAngles.y;
				}
				while(right<left){
					right += 360;
				}
			}
			float midDis = (maxDistance + minDistance) / 2;
			int len = Mathf.FloorToInt((right - left) / 10);
			float degree = (right - left)/len;
			Gizmos.color = Color.cyan;
			for(int i = 0;i<len ; i++){
				//print (i*degree);
				Gizmos.DrawLine(transform.position+Quaternion.Euler(-yMaxLimit,left+i*degree,0)*new Vector3(0,0,midDis),
					transform.position+Quaternion.Euler(-yMaxLimit,left+(i+1)*degree,0)*new Vector3(0,0,midDis));
				Gizmos.DrawLine(transform.position+Quaternion.Euler(-yMinLimit,left+i*degree,0)*new Vector3(0,0,midDis),
					transform.position+Quaternion.Euler(-yMinLimit,left+(i+1)*degree,0)*new Vector3(0,0,midDis));
				
			}

		}
	}

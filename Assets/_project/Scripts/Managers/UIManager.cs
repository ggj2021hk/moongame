using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<UIManager>();
            return _instance;
        }
    }

    public GameObject coverPanel;
    public Button coverStartBtn;
    public GameObject winPanel;
    public GameObject gameverPanel;
    public GameObject thankyouPanel;
    public Slider hpBarSlider;
    public Slider oxygenBarSlider;

    void Awake()
    {
        EventManager.AddListener("gameWin", OnGameWin);
        EventManager.AddListener("gameLose", OnGameLose);
        EventManager.AddListener("thankyou", OnThankyou);
        coverStartBtn.onClick.AddListener(OnClickCoverStartBtn);
        coverPanel.SetActive(true);
    }

    private void OnClickCoverStartBtn()
    {
        coverPanel.SetActive(false);
        PlayerManager.Instance.player.GetComponent<Player>().enabled = true;
    }

    private void OnGameWin(object data)
    {
        winPanel.SetActive(true);
    }

    private void OnGameLose(object data)
    {
        gameverPanel.SetActive(true);
    }

    private void OnThankyou(object data)
    {
        winPanel.SetActive(false);
        gameverPanel.SetActive(false);
        thankyouPanel.SetActive(true);
    }

    public void UpdateHP(float percentage)
    {
        hpBarSlider.value = percentage;
    }
    public void UpdateOxygen(float percentage)
    {
        oxygenBarSlider.value = percentage;
    }


}
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class CollectItemManager : MonoBehaviour
{
    private static CollectItemManager _instance;
    public static CollectItemManager Instance

    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CollectItemManager>();
            return _instance;
        }
    }

    public List<Collection> collectionList = new List<Collection>();

    void Start()
    {

    }

public VehicleTrigger vehicle;
public AudioSource audioSource;
public AudioClip equipmentCollectSound;

    public void CollectItem(Collectable item)
    {
        audioSource.PlayOneShot(equipmentCollectSound);
        switch (item.type)
        {
            case Collectable.Type.Collection:
                string itemId = item.id;
                string[] itemData = itemId.Split(new char[1] { '-' });
                string frontName = itemData[0];
                List<CollectionItem> itemList = collectionList.Find(x => x.name == frontName).itemList;
                itemList.Find(x => x.item.name == item.name).didCollect = true;
                // check if all items collected
                if (itemList.Count(x => x.didCollect) == itemList.Count())
                {
                    // TODO: next action
                    switch (frontName)
                    {
                        case "VehiclePart":
                            Debug.Log("all Vehicle parts collected");
                            vehicle.Ready();
                            break;
                        default:
                            break;
                    }
                }
                break;
            case Collectable.Type.Oxygen:
                break;
            case Collectable.Type.Bomb:
                break;
        }
    }
}

[System.Serializable]
public class Collection
{
    public string name;
    public List<CollectionItem> itemList;
}

[System.Serializable]
public class CollectionItem
{
    public GameObject item;
    public bool didCollect = false;
}
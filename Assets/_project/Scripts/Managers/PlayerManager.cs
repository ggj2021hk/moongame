using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager _instance;
    public static PlayerManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<PlayerManager>();
            return _instance;
        }
    }

    public GameObject player; // it doesn't work like that, when it switches to vehicle

    public float OXYGEN_MAX = 100;
    public float OXYGEN_USE_FACTOR = 1; // increase when runnig?
    public float OXYGEN_SUPPLY_FACTOR = 5;
    public float oxygen;
    public bool didSupplyOxygen = true;

    public float HP_MAX = 100;
    public float hp;

    public Animator escapeRocket;
    public AudioClip breathAlert;
    public AudioClip oxygenRecover;
    public AudioClip dying;
    public AudioSource audioSource;
    private bool alerted;

    bool ended;


    void Start()
    {
        // init default player data
        oxygen = OXYGEN_MAX;
        hp = HP_MAX;
    }

    void OnEnable()
    {
        EventManager.AddListener("kill", OnKill);
        EventManager.AddListener("win", OnWin);
    }

    void OnKill(object data)
    {
        StartCoroutine("WaitGameOver");
        audioSource.PlayOneShot(dying);
        ended = true;
    }

    void OnWin(object data)
    {
        StartCoroutine("WaitGameWin");
        ended = true;
    }

    void OnDisable()
    {
        EventManager.RemoveListener("kill", OnKill);
        EventManager.RemoveListener("win", OnWin);
    }

    private void Update()
    {
        if (ended) return;
        if (!didSupplyOxygen)
        {
            if (oxygen > 0)
            {
                oxygen -= Time.deltaTime * OXYGEN_USE_FACTOR;

                if (!alerted && oxygen < OXYGEN_MAX * 0.54f)
                {
                    audioSource.PlayOneShot(breathAlert);
                    alerted = true;
                }
                if (oxygen <= 0)
                {
                    audioSource.PlayOneShot(dying);
                    EventManager.Trigger("suffocate");
                    StartCoroutine("WaitGameOver");
                }
            }
        }
        else
        {
            oxygen += Time.deltaTime * OXYGEN_SUPPLY_FACTOR;
            if (oxygen > OXYGEN_MAX) oxygen = OXYGEN_MAX;
            if (oxygen > OXYGEN_MAX * 0.7f)
            {
                alerted = false;
            }
        }
        // Debug.Log("oxygen: " + oxygen);
        UIManager.Instance.UpdateOxygen(oxygen / OXYGEN_MAX);
    }

    IEnumerator WaitGameOver()
    {
        yield return new WaitForSeconds(8);
        print("game over scene");
        EventManager.Trigger("gameLose");
        yield return new WaitForSeconds(2);
        EventManager.Trigger("thankyou");
    }

    IEnumerator WaitGameWin()
    {
        yield return new WaitForSeconds(5);
        print("game win scene");
        EventManager.Trigger("gameWin");
        yield return new WaitForSeconds(2);
        EventManager.Trigger("thankyou");
    }

    IEnumerator LaunchRocket()
    {
        EventManager.Trigger("suffocate");
        yield return new WaitForSeconds(8);
    }

    public void TriggerSupplyOxygen(bool enable)
    {
        if (!didSupplyOxygen && enable)
        {
            audioSource.PlayOneShot(oxygenRecover);
        }
        didSupplyOxygen = enable;
    }
}
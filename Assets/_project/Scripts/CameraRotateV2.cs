﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

    public class CameraRotateV2 : MonoBehaviour
    {
		public CameraRotateTarget currentTarget;
		//public bool scaleDragWithDistance;
        public Vector2 dragMultiply = new Vector2(1,1);
        protected float targetDistance;
        protected Vector3 targetAngle;
        protected float currentDistance;
        protected Vector3 currentAngle;
        //private Quaternion targetOrientation;
		
        public float lerpMod = 3;
        
        void OnEnable(){
            SetPosition(transform.position);
        }

		void OnDrawGizmos(){
            Vector3 center = currentTarget? currentTarget.transform.position:Vector3.zero;
			
				Gizmos.DrawWireSphere(center-Quaternion.Euler(targetAngle)*Vector3.forward*targetDistance,1);

		}
		Vector3 lastMousePosition;
		bool refocus;
		void OnApplicationFocus(bool value){
			refocus = true;
		}
		protected virtual bool UpdateInteract(out Vector3 drag){
			drag = new Vector3();
			if(refocus){
				refocus = false;
				lastMousePosition = Input.mousePosition;
				return false;
			}
			bool down = false;
			if( Input.GetMouseButton(0) && !(EventSystem.current!=null && EventSystem.current.IsPointerOverGameObject ()) ){
				Vector3 delta = Input.mousePosition - lastMousePosition ;
				float dis = Vector3.Distance(currentTarget? currentTarget.transform.position:Vector3.zero,transform.position);
				drag .y = delta.x*dragMultiply.x;
				drag.x = delta.y*dragMultiply.y;
				
				down = true;
			}
			drag.z = Input.mouseScrollDelta.y;
			lastMousePosition = Input.mousePosition;
			return down;
		}

        protected virtual void Update()
        {
			Vector3 drag;
			UpdateInteract(out drag);
			targetAngle.y +=drag.y;
			targetAngle.x -=drag.x;
			targetDistance *= 1/(1-drag.z*0.1f);
			if (currentTarget ) {
				targetAngle.x = Mathf.Clamp(targetAngle.x,currentTarget.yMinLimit,currentTarget.yMaxLimit);
				targetDistance = Mathf.Clamp(targetDistance,currentTarget.minDistance,currentTarget.maxDistance);
			}else{
				targetAngle.x = Mathf.Clamp(targetAngle.x,-80,80);
				targetDistance = Mathf.Max(targetDistance,0.1f);
			}
			if (currentTarget && currentTarget.limitX) {
			}
			//

            Vector3 center = currentTarget? currentTarget.transform.position:Vector3.zero;
            Vector3 loc =  transform.position-center;
			
			float lp = 1-Mathf.Pow(Mathf.Pow(0.1f,lerpMod),Time.deltaTime);
			currentDistance = Mathf.Lerp(currentDistance,targetDistance,lp);
			currentAngle = Vector3.Lerp(currentAngle,targetAngle,lp);
			Quaternion rot = Quaternion.Euler(currentAngle);
			transform.rotation = rot;
			transform.position = center-rot*Vector3.forward*currentDistance;
        }

        public void SetPosition(Vector3 idealPos){
            if(currentTarget==null) {
                currentDistance = targetDistance = idealPos.magnitude;
                currentAngle = targetAngle = Quaternion.LookRotation(-idealPos).eulerAngles;
                return;
            }
            Vector3 idealLoc = idealPos- currentTarget.transform.position;
			targetDistance = Mathf.Clamp(idealLoc.magnitude,currentTarget.minDistance,currentTarget.maxDistance);
            targetAngle = Quaternion.LookRotation(-idealLoc).eulerAngles;
            
			if (currentTarget.limitX) {
				float x = ClampAngle(targetAngle.y);
				float tempX = x;
				float left = currentTarget.xLeftLimit;
				float right = currentTarget.xRightLimit;
				if(currentTarget.relative){
					left +=currentTarget. transform.eulerAngles.y;
					right += currentTarget.transform.eulerAngles.y;
				}
				while (right < left) {
					right += 360;
				}
				while (tempX < left) {
					tempX += 360;
				}
				if (tempX >= left && tempX <= right) {
					
				} else if (Quaternion.Angle (Quaternion.Euler (0, x, 0), Quaternion.Euler (0, left, 0)) < Quaternion.Angle (Quaternion.Euler (0, x, 0), Quaternion.Euler (0, right, 0))) {
					x = left;
				} else {
					x = right;
				}

			    float y = Mathf.Clamp(targetAngle.x,currentTarget.yMinLimit,currentTarget.yMaxLimit); 
			    currentAngle = targetAngle = new Vector3(y,x,0);
			}
			currentDistance = targetDistance;
            currentAngle = targetAngle;
        }
		protected static float ClampAngle(float angle, float min =-360, float max=360)
		{
			if (angle < -180)
				angle += 360;
			if (angle > 180)
				angle -= 360;
			return Mathf.Clamp(angle, min, max);
		}

    }


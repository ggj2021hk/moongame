using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresetFootprint : MonoBehaviour
{
    public Vector3[] points;
    public float distance = 1;
    void Start()
    {
		//EventManager.Trigger("step",new object[]{position, transform.rotation});

        if(points.Length>=2 && distance>0){
            Vector3 cur = transform.TransformPoint(points[0]);
            Vector3 dir =Vector3.forward;
            int i = 1;

            //while(Vector3.Distance(cur,transform.TransformPoint(points[i])) <distance){
             while(true){
                if(Vector3.Distance(cur,transform.TransformPoint(points[i])) <distance){
                    i++;
                    if(i== points.Length){
                        break;
                    }
                }
                dir = transform.TransformPoint(points[i])-cur;
			    EventManager.Trigger("step",new object[]{cur, Quaternion.LookRotation(dir)});
                cur = Vector3.MoveTowards(cur,transform.TransformPoint(points[i]),distance);
            }
			    EventManager.Trigger("step",new object[]{cur, Quaternion.LookRotation(dir)});
        }
    }

    void OnDrawGizmos(){
        if(points.Length>=2){
            for(int i = 0 ; i< points.Length-1 ; i ++){
                Gizmos.DrawLine(transform.TransformPoint(points[i]),transform.TransformPoint(points[i+1]));
            } 
        }
    }
    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingTrigger : MonoBehaviour
{
    public Animator rocket;
    public FollowTransform camSet;
    public AudioSource audio;
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Player") return;
        other.gameObject.SetActive(false);
        camSet.enabled = false;
        camSet.transform.position = transform.position;
        rocket.enabled = true;
        EventManager.Trigger("win");
        audio.enabled = true;

    }
}

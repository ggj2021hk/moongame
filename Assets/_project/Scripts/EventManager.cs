using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EventManager
{
    private static Dictionary<string,List<Action<object>>> records = new Dictionary<string,List<Action<object>>>();

    public static void AddListener(string eventName,Action<object> feedback){
            if(!records.ContainsKey(eventName)){
                records.Add(eventName,new List<Action<object>>());
            }
            List<Action<object>> list = records[eventName];
            list.Add(feedback);
    }
    public static void RemoveListener(string eventName,Action<object> feedback){
        if(records.ContainsKey(eventName)){
            List<Action<object>> list = records[eventName];
            list.Remove(feedback);
        }
    }
    public static void ClearListeners(string eventName){
        records.Remove(eventName);
    }
    public static void ClearListeners(){
         records = new Dictionary<string,List<Action<object>>>();

    }
    public static void Trigger(string eventName, object data = null){
        if(records.ContainsKey(eventName)){
            List<Action<object>> list = records[eventName];
           // foreach( Action<object> action in list){
            for(int i = 0 ; i< list.Count ; i ++){
                list[i].Invoke(data);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDeformer : MonoBehaviour
{
    public float radius = 10;
    
    public AnimationCurve curve;
    public float curveMultiple = 1;
    public bool clampBottom;
    public float noise =0;
    public CapsuleCollider capsuleCollider;
    
    void OnDrawGizmos(){
        Gizmos.color = Color.grey;
        for(int i =0 ; i < 12 ; i ++){
        Gizmos.DrawLine(transform.position+Quaternion.Euler(0,i*360/12,0)*Vector3.forward*radius,transform.position+Quaternion.Euler(0,(i+1)*360/12,0)*Vector3.forward*radius);
        }
    }

    void OnValidate(){
        if(capsuleCollider)capsuleCollider.radius = radius;
    }
}

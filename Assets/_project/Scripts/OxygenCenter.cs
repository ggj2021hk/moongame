using UnityEngine;

public class OxygenCenter : MonoBehaviour
{
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter: " + other.gameObject.name);
        if (other.gameObject.tag == "Player") 
        {
            Debug.Log("found player");
            PlayerManager.Instance.TriggerSupplyOxygen(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("OnTriggerExit: " + other.gameObject.name);
        if (other.gameObject.tag == "Player") 
        {
            Debug.Log("lost player");
            PlayerManager.Instance.TriggerSupplyOxygen(false);
        }
    }
}
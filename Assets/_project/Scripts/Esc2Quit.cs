﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Esc2Quit : MonoBehaviour {
	public enum Action
	{
		BackToMenu,
		Quit
	}
	public Action action;
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (action == Action.BackToMenu) {
				SceneManager.LoadScene ("Menu");
			} else {
				
				Application.Quit ();
			}
		}
	}
}

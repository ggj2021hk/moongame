﻿using UnityEngine;
using System.Collections;

public class FollowTransform : MonoBehaviour {
	public Transform target;
	public bool followPosition = true;
	public Vector3 positionOffset;
	public bool followRotation;
	public Vector3 rotationOffset;
	public enum UpdateTime
	{
		Update,
		LateUpdate
	}
	public UpdateTime updateTime;

	void Update () {
		if(updateTime == UpdateTime.Update){
			updateTransform ();
		}
	}
	void LateUpdate () {
		if(updateTime == UpdateTime.LateUpdate){
			updateTransform ();
		}
	}
	void updateTransform(){
		if (followPosition) {
			transform.position = target.position + positionOffset;
		}
		if (followRotation) {
			transform.eulerAngles = target.eulerAngles + rotationOffset;
		}

	}
}

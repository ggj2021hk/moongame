using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerFX : MonoBehaviour
{
    public float lifespan = 1;
    public float rotateSpeed = 180;
    public AnimationCurve alpha;
    public SpriteRenderer renderer;
    private float timer ;
    private Color colorCache;
    // Start is called before the first frame update
    void Start()
    {
        colorCache = renderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        renderer.transform.localEulerAngles = new Vector3(90,Time.time*rotateSpeed,0);
        colorCache.a = alpha.Evaluate(timer/lifespan);
        renderer.color = colorCache;
        timer+=Time.deltaTime;
        if(timer>lifespan){
            Destroy(gameObject);
        }
    }
}
